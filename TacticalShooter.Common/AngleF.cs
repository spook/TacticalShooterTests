﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public struct AngleF : IAngleF
    {
        private float value;

        public AngleF(float value)
        {
            this.value = value;
        }

        public AngleF Add(IAngleF angle)
        {
            return new AngleF(value + angle.Value);
        }

        public AngleF Add(float angle)
        {
            return new AngleF(value + angle);
        }

        public AngleF Subtract(IAngleF angle)
        {
            return new AngleF(value - angle.Value);
        }

        public AngleF Subtract(float angle)
        {
            return new AngleF(value - angle);
        }

        public AngleF ShortestDiffTo(IAngleF angle)
        {
            return this.EvalShortestDiffTo(angle);
        }

        public override string ToString()
        {
            return $"Angle, {value:0.##}°";
        }

        public float Value => value;

        public float ValueRad => (float)(value * Math.PI / 180.0f);

        public IAngleF Normalized => new NormalizedAngleF(value);
    }
}
