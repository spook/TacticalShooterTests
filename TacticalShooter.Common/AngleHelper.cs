﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    internal static class AngleHelper
    {
        public static AngleF EvalShortestDiffTo(this IAngleF thisAngle, IAngleF angle)
        {
            float diff = angle.Normalized.Value - thisAngle.Normalized.Value;
            while (diff > 180.0f)
                diff -= 360.0f;
            while (diff < -180.0f)
                diff += 360.0f;

            return new AngleF(diff);
        }
    }
}
