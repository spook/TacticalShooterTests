﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public interface IAngleF
    {
        AngleF ShortestDiffTo(IAngleF angle);

        float Value { get; }
        float ValueRad { get; }
        IAngleF Normalized { get; }
    }
}
