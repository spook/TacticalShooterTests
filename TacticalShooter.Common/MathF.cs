﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public class MathF
    {
        private const float ZERO = 1.0e-10f;

        public static bool IsZero(float f)
        {
            return Math.Abs(f) < ZERO;
        }

        public static float Clamp(float min, float value, float max)
        {
            if (min > max)
                throw new ArgumentException("Invalid border values!");

            return Math.Min(max, Math.Max(min, value));
        }

        public static (float xLower, float xHigher) SolveQuadratic(float a, float b, float c)
        {
            float delta = b * b - 4 * a * c;
            if (delta < 0.0f)
                return (float.NaN, float.NaN);
            if (delta > 0.0f && delta < ZERO)
                return (-b / 2 * a, float.NaN);

            float x1 = (-b - (float)Math.Sqrt(delta)) / (2 * a);
            float x2 = (-b + (float)Math.Sqrt(delta)) / (2 * a);

            return (Math.Min(x1, x2), Math.Max(x1, x2));
        }

        public static (float x, float y) SolveDoubleEquations(float a1, float b1, float c1, float a2, float b2, float c2)
        {
            float W = a1 * b2 - a2 * b1;
            if (IsZero(W))
                return (float.NaN, float.NaN);

            float Wx = c1 * b2 - c2 * b1;
            float Wy = a1 * c2 - a2 * c1;

            return (Wx / W, Wy / W);
        }

        /// <summary>
        /// Evaluates orthogonal projection of point to a vector
        /// </summary>
        /// <param name="pointF">Point to be projected</param>
        /// <param name="vector">Projection vector</param>
        /// <returns>Returns such value t, that t * vector = projection of point to vector</returns>
        public static float EvalProjection(PointF point, VectorF vector)
        {
            VectorF normal = new VectorF(vector.DY, -vector.DX);

            // t * vector.DX + s * normal.DX = point.DX
            // t * vector.DY + s * normal.DY = point.DY

            (float t, _) = SolveDoubleEquations(vector.DX, normal.DX, point.X, vector.DY, normal.DY, point.Y);

            return t;
        }

        public static (PointF center, float radius) FindCircle(PointF A, PointF B, PointF C)
        {
            PointF ABCenter = new PointF((A.X + B.X) / 2.0f, (A.Y + B.Y) / 2.0f);
            PointF BCCenter = new PointF((B.X + C.X) / 2.0f, (B.Y + C.Y) / 2.0f);

            VectorF ABNormal = new VectorF(A.X - B.X, B.Y - A.Y);
            VectorF BCNormal = new VectorF(B.X - C.X, C.Y - B.Y);

            // ABCenter + t * ABNormal = BCCenter + s * BCNormal
            // or
            // t * ABNormal - s * BCNormal = BCCenter - ABCenter

            (float t, _) = SolveDoubleEquations(ABNormal.DX, -BCNormal.DY, BCCenter.X - ABCenter.X,
                ABNormal.DY, -BCNormal.DY, BCCenter.Y - ABCenter.Y);

            PointF center = ABCenter.Add(ABNormal.Multiply(t));
            float radius = new VectorF(center.X - A.X, center.Y - A.Y).Length;

            return (center, radius);
        }
    }
}
