﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public struct NormalizedAngleF : IAngleF
    {
        private float value;

        private void Normalize()
        {
            float mul = (float)Math.Floor(value / 360.0f);
            value -= mul * 360.0f;

            while (value >= 360.0f)
                value -= 360.0f;
            while (value < 0.0f)
                value += 360.0f;
        }

        public NormalizedAngleF(float value)
        {
            this.value = value;
            Normalize();
        }

        public NormalizedAngleF Add(IAngleF angle)
        {
            return new NormalizedAngleF(value + angle.Value);
        }

        public NormalizedAngleF Add(float angle)
        {
            return new NormalizedAngleF(value + angle);
        }

        public NormalizedAngleF Subtract(IAngleF angle)
        {
            return new NormalizedAngleF(value - angle.Value);
        }

        public NormalizedAngleF Subtract(float angle)
        {
            return new NormalizedAngleF(value - angle);
        }

        public AngleF ShortestDiffTo(IAngleF angle)
        {
            return this.EvalShortestDiffTo(angle);
        }

        public VectorF ToVector(float length)
        {
            if (length > 0.0f)
            {
                return VectorF.FromPolar(Math.Abs(length), this);
            }
            else
            {
                return VectorF.FromPolar(Math.Abs(length), this.Add(180.0f));
            }
        }

        public override string ToString()
        {
            return $"Normalized Angle, {value:0.##}°";
        }

        public float Value => value;

        public float ValueRad => (float)(value * Math.PI / 180.0f);

        public IAngleF Normalized => this;
    }
}
