﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public static class PointFExtensions
    {
        public static PointF Add(this PointF point, VectorF vector)
        {
            return new PointF(point.X + vector.DX, point.Y + vector.DY);
        }

        public static PointF Subtract(this PointF point, VectorF vector)
        {
            return new PointF(point.X - vector.DX, point.Y - vector.DY);
        }

        public static VectorF Subtract(this PointF point, PointF point2)
        {
            return new VectorF(point.X - point2.X, point.Y - point2.Y);
        }

        public static PointF RotateLeft(this PointF point, IAngleF angle)
        {
            float x = (float)Math.Cos(angle.ValueRad) * point.X - (float)Math.Sin(angle.ValueRad) * point.Y;
            float y = (float)Math.Sin(angle.ValueRad) * point.X + (float)Math.Cos(angle.ValueRad) * point.Y;

            return new PointF(x, y);
        }


        public static PointF RotateRight(this PointF point, IAngleF angle)
        {
            return RotateLeft(point, new AngleF(-angle.Value));
        }
    }
}
