﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    public struct VectorF
    {
        public VectorF(float dx, float dy)
        {
            DX = dx;
            DY = dy;
        }

        public VectorF(VectorF vector)
        {
            DX = vector.DX;
            DY = vector.DY;
        }

        public static VectorF FromPolar(float r, IAngleF angle)
        {
            if (r < 0)
                throw new ArgumentException("Radius must be positive!");

            float dx = r * (float)Math.Sin(angle.ValueRad);
            float dy = r * (float)Math.Cos(angle.ValueRad);

            return new VectorF(dx, dy);
        }

        public VectorF Add(VectorF vector)
        {
            return new VectorF(DX + vector.DX, DY + vector.DY);
        }

        public VectorF Subtract(VectorF vector)
        {
            return new VectorF(DX - vector.DX, DY - vector.DY);
        }

        public VectorF Multiply(float value)
        {
            return new VectorF(DX * value, DY * value);
        }

        public VectorF Rotate(NormalizedAngleF angle)
        {
            float newX = (float)(DX * Math.Cos(angle.ValueRad) - DY * Math.Sin(angle.ValueRad));
            float newY = (float)(DX * Math.Sin(angle.ValueRad) + DY * Math.Cos(angle.ValueRad));

            return new VectorF(newX, newY);
        }

        public PointF Apply(PointF point)
        {
            return new PointF(point.X + DX, point.Y + DY);
        }

        public VectorF WithLength(float length)
        {
            float len = Length;
            if (MathF.IsZero(len))
                throw new InvalidOperationException("Cannot resize zero-length vector!");

            return new VectorF(DX / len * length, DY / len * length);
        }

        public float DotProductWith(VectorF vector)
        {
            return this.EvalDotProductWith(vector);
        }

        public VectorF ProjectTo(VectorF vector)
        {
            return this.EvalProjectTo(vector);
        }

        public override string ToString()
        {
            return $"Vector, (x: {DX:0.##}, y: {DY:0.##}, angle: {Angle.Value:0.##}°, length: {Length:0.##}";
        }

        public float DX { get; private set; }
        public float DY { get; private set; }

        public NormalizedAngleF Angle
        {
            get
            {
                if (MathF.IsZero(DX) && MathF.IsZero(DY))
                    return new NormalizedAngleF(0.0f);
                else
                {
                    float alpha = (float)Math.Acos(DY / Length);
                    if (DX < 0)
                        alpha = (float)(2 * Math.PI - alpha);

                    return new NormalizedAngleF((float)(alpha * 180.0f / Math.PI));
                }
            }
        }

        public float Length
        {
            get
            {
                return ((float)Math.Sqrt(DX * DX + DY * DY));
            }
        }
    }
}
