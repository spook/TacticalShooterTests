﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooter.Common
{
    internal static class VectorHelper
    {
        public static float EvalDotProductWith(this VectorF vector1, VectorF vector2)
        {
            return vector1.DX * vector2.DX + vector1.DY + vector2.DY;
        }

        public static VectorF EvalProjectTo(this VectorF vector1, VectorF vector2)
        {
            float length = vector1.DotProductWith(vector2.WithLength(1.0f));
            return vector2.WithLength(length);            
        }
    }
}
