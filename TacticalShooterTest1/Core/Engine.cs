﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Core
{
    public static class Engine
    {
        public static SimulationResult Simulate(StartingParams startingParams, IPilot pilot)
        {
            List<State> states = new List<State>();
            List<Decision> decisions = new List<Decision>();

            State state = new State(startingParams.StartingPoint, 
                VectorF.FromPolar(startingParams.StartingSpeed, startingParams.StartingAngle), 
                startingParams.StartingAngle);

            states.Add(state);

            float maxThrustPerStep = startingParams.MaxThrust / startingParams.StepsPerSecond;
            float maxReverseThrustPerStep = startingParams.MaxReverseThrust / startingParams.StepsPerSecond;

            float maxLeftRotationPerStep = startingParams.MaxLeftRotation / startingParams.StepsPerSecond;
            float maxRightRotationPerStep = startingParams.MaxRightRotation / startingParams.StepsPerSecond;

            for (int i = 0; i < startingParams.Seconds; i++)
            {
                for (int j = 0; j < startingParams.StepsPerSecond; j++)
                {
                    Decision decision = pilot.Decide(startingParams, state);
                    decisions.Add(decision);

                    float rotationChange = MathF.Clamp(maxLeftRotationPerStep, decision.OrientationChange, maxRightRotationPerStep);
                    float speedChange = MathF.Clamp(maxReverseThrustPerStep, decision.SpeedChange, maxThrustPerStep);

                    NormalizedAngleF newAngle = state.Orientation.Add(rotationChange);
                    VectorF newSpeed = state.Speed.Add(VectorF.FromPolar(speedChange, newAngle));

                    PointF newPosition;
                    if (!MathF.IsZero(newSpeed.Length))
                    {
                        newSpeed = newSpeed.WithLength(MathF.Clamp(-startingParams.MaxSpeed, newSpeed.Length, startingParams.MaxSpeed));
                        VectorF scaledSpeed = newSpeed.WithLength(newSpeed.Length / startingParams.StepsPerSecond);
                        newPosition = state.Position.Add(scaledSpeed);
                    }
                    else
                    {
                        // No speed means no position change
                        newPosition = state.Position;
                    }

                    state = new State(newPosition, newSpeed, newAngle);
                    states.Add(state);
                }
            }

            return new SimulationResult(startingParams, states, decisions);
        }
    }
}
