﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooterTest1.Core.Engines.Realistic
{
    public class Decision
    {
        public Decision(float speedChange, float rotationChange)
        {
            SpeedChange = speedChange;
            RotationChange = rotationChange;
        }

        public float SpeedChange { get; private set; }
        public float RotationChange { get; private set; }        
    }
}
