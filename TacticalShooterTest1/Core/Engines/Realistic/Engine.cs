﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooterTest1.Common;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Core.Engines.Realistic
{
    public static class Engine
    {
        public static SimulationResult Simulate(StartingParams startingParams, IPilot pilot)
        {
            State state = new State(startingParams.StartingPoint, 
                new PolarVectorF(startingParams.StartingSpeed, startingParams.StartingAngle), 
                startingParams.StartingAngle, 
                0.0f);

            List<State> states = new List<State>();
            states.Add(state);

            float maxSpeedChangePerStep = startingParams.MaxThrust / (float)startingParams.StepsPerSecond;
            float maxRotationChangePerStep = startingParams.MaxRotationChange / (float)startingParams.StepsPerSecond;

            for (int i = 0; i < startingParams.Seconds; i++)
            {
                for (int j = 0; j < startingParams.StepsPerSecond; j++)
                {
                    Decision decision = pilot.Decide(startingParams, state);

                    float rotationChange = MathF.Clamp(-maxRotationChangePerStep, decision.RotationChange, maxRotationChangePerStep);
                    float speedChange = MathF.Clamp(-maxSpeedChangePerStep, decision.SpeedChange, maxSpeedChangePerStep);

                    float newRotation = MathF.Clamp(-startingParams.MaxRotation, state.Rotation + rotationChange, startingParams.MaxRotation);
                    float scaledRotation = newRotation / startingParams.StepsPerSecond;
                    AngleF newAngle = state.Orientation.Add(scaledRotation);
                    PolarVectorF newSpeed = state.Speed.Add(new PolarVectorF(speedChange, newAngle));
                    newSpeed = newSpeed.WithRadius(MathF.Clamp(-startingParams.MaxSpeed, newSpeed.R, startingParams.MaxSpeed));

                    PolarVectorF scaledSpeed = newSpeed.WithRadius(newSpeed.R / startingParams.StepsPerSecond);
                    PointF newPosition = state.Position.Add(scaledSpeed);

                    state = new State(newPosition, newSpeed, newAngle, newRotation);
                    states.Add(state);
                }
            }

            return new SimulationResult(states);
        }
    }
}
