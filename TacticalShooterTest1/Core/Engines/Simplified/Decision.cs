﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooterTest1.Core.Engines.Simplified
{
    public class Decision
    {
        public Decision(float thrust, float rotation)
        {
            Thrust = thrust;
            Rotation = rotation;
        }

        public float Thrust { get; private set; }
        public float Rotation { get; private set; }        
    }
}
