﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Core.Engines.Realistic
{
    public interface IPilot
    {
        Decision Decide(StartingParams startingParams, IState currentConditions);        
    }
}
