﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Core
{
    public interface IPilot
    {
        Decision Decide(StartingParams startingParams, IState currentConditions);
        void DrawDebug(State state, Decision decision, Graphics g);
    }
}
