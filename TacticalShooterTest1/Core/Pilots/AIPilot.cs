﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Core.Pilots
{
    public class AIPilot : IPilot
    {
        private VectorF EvalDesiredVector(StartingParams startingParams, IState currentState)
        {
            // Allows remodeling the optimal trajectory below ship's capabilities.
            // This adds a room to maneuver, making reaching target easier.
            // Set the value below 1.0 to enable.
            float desiredReverseThrust = startingParams.MaxReverseThrust * 1.0f;

            float brakingTime = -(startingParams.MaxSpeed / desiredReverseThrust);
            float brakingDistance = brakingTime * startingParams.MaxSpeed + (desiredReverseThrust * brakingTime * brakingTime) / 2.0f;

            VectorF shipToTarget = startingParams.TargetPoint.Subtract(currentState.Position);
            float distance = shipToTarget.Length;

            VectorF desiredVector;
            if (distance > brakingDistance)
            {
                // Desired vector is towards target with maximum speed
                desiredVector = VectorF.FromPolar(startingParams.MaxSpeed, shipToTarget.Angle);
            }
            else if (MathF.IsZero(distance))
            {
                desiredVector = VectorF.FromPolar(0.0f, shipToTarget.Angle);
            }
            else
            {
                // Ship should be braking to reach target
                (float t, _) = MathF.SolveQuadratic(desiredReverseThrust / 2, startingParams.MaxSpeed, -(brakingDistance - distance));

                if (float.IsNaN(t))
                {
                    // Border point between max speed and starting braking
                    desiredVector = VectorF.FromPolar(startingParams.MaxSpeed, shipToTarget.Angle);
                }
                else
                {
                    float desiredSpeed = startingParams.MaxSpeed + desiredReverseThrust * t;
                    desiredVector = VectorF.FromPolar(desiredSpeed, shipToTarget.Angle);
                }                    
            }

            return desiredVector;
        }

        /// <summary>
        /// Evaluates orthogonal projection of point to a vector
        /// </summary>
        /// <param name="pointF">Point to be projected</param>
        /// <param name="vector">Projection vector</param>
        /// <returns>Returns such value t, that t * vector = projection of point to vector</returns>
        private float EvalProjection(PointF point, VectorF vector)
        {
            VectorF normal = new VectorF(vector.DY, -vector.DX);

            // t * vector.DX + s * normal.DX = point.DX
            // t * vector.DY + s * normal.DY = point.DY

            (float t, _) = MathF.SolveDoubleEquations(vector.DX, normal.DX, point.X, vector.DY, normal.DY, point.Y);

            return t;
        }

        public Decision Decide(StartingParams startingParams, IState currentConditions)
        {
            VectorF shipToTarget = startingParams.TargetPoint.Subtract(currentConditions.Position);

            if (shipToTarget.Length < 5)
            {
                // Attempt to stop the ship

                AngleF diff;
                
                if (!MathF.IsZero(currentConditions.Speed.Length)) {
                    diff = currentConditions.Orientation.ShortestDiffTo(currentConditions.Speed.Angle);
                    if (Math.Abs(diff.Value) > 90.0f)
                        diff = new AngleF(diff.Value - 180.0f * Math.Sign(diff.Value));
                }
                else 
                {
                    diff = new AngleF(0.0f);
                }

                float thrust;
                if (Math.Abs(diff.Value) < 1)
                {
                    // Project thrust to speed, then reverse to stop
                    VectorF thrustVector = VectorF.FromPolar(1, currentConditions.Orientation);
                    PointF speedPoint = new PointF(currentConditions.Speed.DX, currentConditions.Speed.DY);

                    float t = EvalProjection(speedPoint, thrustVector);

                    thrust = -t;
                }
                else
                {
                    thrust = 0.0f;
                }

                AngleF rotation = new AngleF(MathF.Clamp(startingParams.MaxLeftRotation / startingParams.StepsPerSecond, diff.Value, startingParams.MaxRightRotation / startingParams.StepsPerSecond));
                thrust = MathF.Clamp(startingParams.MaxReverseThrust / startingParams.StepsPerSecond, thrust, startingParams.MaxThrust / startingParams.StepsPerSecond);

                return new Decision(thrust, rotation.Value, null);
            }
            else
            {
                // Attempt to navigate to target

                VectorF desiredVector = EvalDesiredVector(startingParams, currentConditions);
                if (MathF.IsZero(desiredVector.Length))
                    return new Decision(0.0f, 0.0f);
                VectorF speedVector = currentConditions.Speed;

                // Rotation

                VectorF targetOrientation = desiredVector.Subtract(speedVector);
                AngleF diff = currentConditions.Orientation.ShortestDiffTo(targetOrientation.Angle);

                // We can use reverse thrust instead of forward, less rotation is better (quicker response)
                if (Math.Abs(diff.Value) > 90.0f)
                    diff = new AngleF(diff.Value - 180.0f * Math.Sign(diff.Value));

                AngleF rotation = new AngleF(MathF.Clamp(startingParams.MaxLeftRotation / startingParams.StepsPerSecond,
                    diff.Value / startingParams.StepsPerSecond,
                    startingParams.MaxRightRotation / startingParams.StepsPerSecond));

                VectorF thrustVector = VectorF.FromPolar(1.0f, new AngleF(currentConditions.Orientation.Value + rotation.Value));

                // Thrust

                VectorF translatedDesiredVector = desiredVector.Subtract(speedVector);
                float optimalThrust = EvalProjection(new PointF(translatedDesiredVector.DX, translatedDesiredVector.DY), thrustVector);

                AIPilotDebugData data = new AIPilotDebugData(shipToTarget, desiredVector, speedVector, optimalThrust);

                float optimalThrustValue = MathF.Clamp(startingParams.MaxReverseThrust / startingParams.StepsPerSecond,
                    optimalThrust / startingParams.StepsPerSecond,
                    startingParams.MaxThrust / startingParams.StepsPerSecond);

                return new Decision(optimalThrust, rotation.Value, data);
            }
        }

        public void DrawDebug(State state, Decision decision, Graphics g)
        {
            AIPilotDebugData data = decision.Data as AIPilotDebugData;
            if (data == null)
                return;

            // g.DrawLine(Pens.Orange, state.Position.X, state.Position.Y, state.Position.X + data.ShipToTarget.DX, state.Position.Y + data.ShipToTarget.DY);
            g.DrawLine(Pens.Violet, state.Position.X, state.Position.Y, state.Position.X + data.DesiredVector.DX, state.Position.Y + data.DesiredVector.DY);
            g.DrawLine(Pens.ForestGreen, state.Position.X, state.Position.Y, state.Position.X + data.SpeedVector.DX, state.Position.Y + data.SpeedVector.DY);

            VectorF thrust = VectorF.FromPolar(1, state.Orientation);
            thrust = new VectorF(thrust.DX * data.Thrust, thrust.DY * data.Thrust);
            g.DrawLine(Pens.Orange, state.Position.X, state.Position.Y, state.Position.X + thrust.DX, state.Position.Y + thrust.DY);
        }
    }
}
