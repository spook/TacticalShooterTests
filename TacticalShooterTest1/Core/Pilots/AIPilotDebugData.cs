﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest1.Core.Pilots
{
    public class AIPilotDebugData
    {
        public AIPilotDebugData(VectorF shipToTarget, VectorF desiredVector, VectorF speedVector, float thrust)
        {
            ShipToTarget = shipToTarget;
            DesiredVector = desiredVector;
            SpeedVector = speedVector;
            Thrust = thrust;
        }

        public VectorF ShipToTarget { get; }
        public VectorF DesiredVector { get; }
        public VectorF SpeedVector { get; }
        public float Thrust { get; }
    }
}
