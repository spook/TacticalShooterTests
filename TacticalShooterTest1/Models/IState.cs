﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest1.Models
{
    public interface IState
    {
        PointF Position { get; }
        VectorF Speed { get; }
        NormalizedAngleF Orientation { get; }
    }
}
