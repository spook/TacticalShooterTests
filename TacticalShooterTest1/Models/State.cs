﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest1.Models
{
    public class State : IState
    {
        public State(PointF position, VectorF speed, NormalizedAngleF angle)
        {
            Position = position;
            Speed = speed;
            Orientation = angle;
        }

        public PointF Position { get; private set; }
        public VectorF Speed { get; private set; }
        public NormalizedAngleF Orientation { get; private set; }
    }
}
