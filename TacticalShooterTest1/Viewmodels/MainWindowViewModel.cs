﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TacticalShooter.Common;
using TacticalShooterTest1.Core;
using TacticalShooterTest1.Core.Pilots;
using TacticalShooterTest1.Models;

namespace TacticalShooterTest1.Viewmodels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        // Private fields -----------------------------------------------------

        private IMainWindowAccess access;

        private float startingPointX = 0.0f;
        private float startingPointY = 0.0f;
        private float targetPointX = 100.0f;
        private float targetPointY = 100.0f;
        private float startingAngle = 0.0f;
        private float startingSpeed = 10.0f;
        private float maxThrust = 10.0f;
        private float maxSpeed = 500.0f;
        private float maxRotation = 90.0f;
        private int seconds = 1;
        private int stepsPerSecond = 100;
        private int currentStep = 0;

        private SimulationResult simulationResult;

        private readonly IPilot pilot = new AIPilot();

        public MainWindowViewModel(IMainWindowAccess access)
        {
            this.access = access;
        }

        // Private methods ----------------------------------------------------

        private void DrawShip(State state, Graphics g, Brush brush)
        {
            VectorF vector1 = VectorF.FromPolar(8, state.Orientation);
            VectorF vector2 = VectorF.FromPolar(8, state.Orientation.Subtract(135));
            VectorF vector3 = VectorF.FromPolar(8, state.Orientation.Add(135));

            PointF[] points = new[] {
                state.Position.Add(vector1),
                state.Position.Add(vector2),
                state.Position.Add(vector3)
            };

            g.FillPolygon(brush, points);
        }

        internal void SetTargetPoint(System.Windows.Point point)
        {
            SizeF size = access.GetPreviewSize();
            PointF newTarget = new PointF((float)(point.X - size.Width / 2.0f), (float)(-(point.Y - size.Height / 2.0f)));

            targetPointX = newTarget.X;
            targetPointY = newTarget.Y;

            EvalChanges();
        }

        private void DrawSimulationResult(Graphics g)
        {
            DrawPath(g);
            DrawShipGhosts(g);
            DrawCurrentStep(g);
        }

        private void DrawCurrentStep(Graphics g)
        {
            using (Brush b = new SolidBrush(Color.FromArgb(255, Color.Blue)))
            {
                State state = simulationResult.States[currentStep];
                Decision decision = simulationResult.Decisions[currentStep];
                DrawShip(state, g, b);

                pilot.DrawDebug(state, decision, g);
            }
        }

        private void DrawShipGhosts(Graphics g)
        {
            using (Brush b = new SolidBrush(Color.FromArgb(32, Color.Blue)))
                for (int i = 0; i < simulationResult.States.Count; i += Math.Max(1, simulationResult.States.Count / 10))
                {
                    DrawShip(simulationResult.States[i], g, b);
                }
        }

        private void DrawPath(Graphics g)
        {            
            GraphicsPath path = new GraphicsPath();
            for (int i = 0; i < simulationResult.States.Count - 1; i++)
                path.AddLine(new System.Drawing.Point((int)simulationResult.States[i].Position.X, (int)simulationResult.States[i].Position.Y),
                    new System.Drawing.Point((int)simulationResult.States[i + 1].Position.X, (int)simulationResult.States[i + 1].Position.Y));

            using (Pen p = new Pen(new SolidBrush(Color.FromArgb(32, Color.Red))))
                g.DrawPath(p, path);
        }

        private void EvalChanges()
        {
            currentStep = Math.Min(MaxSteps - 1, currentStep);
            OnPropertyChanged("CurrentStep");

            OnPropertyChanged("MaxSteps");

            StartingParams startingParams = new StartingParams(
                new PointF(startingPointX, startingPointY),
                new NormalizedAngleF(startingAngle),
                startingSpeed,
                maxThrust,
                maxSpeed,
                maxRotation,
                new PointF(targetPointX, targetPointY),
                stepsPerSecond,
                seconds);

            simulationResult = Engine.Simulate(startingParams, pilot);
            DrawResult();
        }

        private void DrawResult()
        {
            if (simulationResult != null) { 

                SizeF size = access.GetPreviewSize();

                Bitmap bitmap = new Bitmap((int)size.Width, (int)size.Height, PixelFormat.Format24bppRgb);

                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.SmoothingMode = SmoothingMode.AntiAlias;

                    g.FillRectangle(Brushes.White, new Rectangle(0, 0, (int)size.Width, (int)size.Height));
                    g.TranslateTransform(size.Width / 2.0f, size.Height / 2.0f);
                    g.ScaleTransform(1.0f, -1.0f);

                    DrawTarget(g);

                    DrawSimulationResult(g);
                }

                access.DrawPreview(bitmap);
            }
        }

        private void DrawTarget(Graphics g)
        {
            PointF targetPoint = simulationResult.StartingParams.TargetPoint;

            g.DrawLine(Pens.BlueViolet, targetPoint.X, targetPoint.Y - 5, targetPoint.X, targetPoint.Y + 5);
            g.DrawLine(Pens.BlueViolet, targetPoint.X - 5, targetPoint.Y, targetPoint.X + 5, targetPoint.Y);
        }

        // Protected methods --------------------------------------------------

        protected void OnPropertyChanged(string property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        // Public properties --------------------------------------------------

        public float StartingPointX { get => startingPointX; set { startingPointX = value; EvalChanges(); } }
        public float StartingPointY { get => startingPointY; set { startingPointY = value; EvalChanges(); } }
        public float TargetPointX { get => targetPointX; set { targetPointX = value; EvalChanges(); } }
        public float TargetPointY { get => targetPointY; set { targetPointY = value; EvalChanges(); } }
        public float StartingAngle { get => startingAngle; set { startingAngle = value; EvalChanges(); } }
        public float StartingSpeed { get => startingSpeed; set { startingSpeed = value; EvalChanges(); } }
        public float MaxThrust { get => maxThrust; set { maxThrust = value; EvalChanges(); } }
        public float MaxSpeed { get => maxSpeed; set { maxSpeed = value; EvalChanges(); } }
        public float MaxRotation { get => maxRotation; set { maxRotation = value; EvalChanges(); } }
        public int Seconds { get => seconds; set { seconds = value; EvalChanges(); } }
        public int StepsPerSecond { get => stepsPerSecond; set { stepsPerSecond = value; EvalChanges(); } }
        public int MaxSteps => seconds * stepsPerSecond - 1;
        public int CurrentStep { get => currentStep; set { currentStep = value; DrawResult(); } }

        // Public events ------------------------------------------------------

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
