﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest2.Models;

namespace TacticalShooterTest2.Core
{
    public static class Engine
    {
        public static SimulationResult Simulate(StartingParams startingParams, IPilot pilot)
        {
            List<State> states = new List<State>();
            List<Decision> decisions = new List<Decision>();

            State state = new State(startingParams.StartingPoint, startingParams.StartingAngle, startingParams.StartingSpeed);

            states.Add(state);

            for (int i = 0; i < startingParams.Seconds; i++)
            {
                for (int j = 0; j < startingParams.StepsPerSecond; j++)
                {
                    Decision decision = pilot.Decide(startingParams, state);
                    decisions.Add(decision);

                    float rotationChange = MathF.Clamp(startingParams.MaxLeftRotationPerStep, decision.OrientationChange, startingParams.MaxRightRotationPerStep);                    
                    float speedChange = MathF.Clamp(startingParams.MaxReverseThrustPerStep, decision.SpeedChange, startingParams.MaxThrustPerStep);

                    NormalizedAngleF newOrientation = state.Orientation.Add(rotationChange);
                    float newSpeed = MathF.Clamp(-startingParams.MaxSpeed, state.Speed + speedChange, startingParams.MaxSpeed);

                    PointF newPosition;
                    if (!MathF.IsZero(state.Speed))
                    {                                
                        VectorF scaledSpeed = state.Orientation.ToVector(state.Speed / startingParams.StepsPerSecond);
                        newPosition = state.Position.Add(scaledSpeed);
                    }
                    else
                    {
                        // No speed means no position change
                        newPosition = state.Position;
                    }

                    state = new State(newPosition, newOrientation, newSpeed);
                    states.Add(state);
                }
            }

            return new SimulationResult(startingParams, states, decisions);
        }
    }
}
