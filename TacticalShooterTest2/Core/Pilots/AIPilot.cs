﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest2.Models;

namespace TacticalShooterTest2.Core.Pilots
{
    public class AIPilot : IPilot
    {
        private (float left, float right) EvalSpeedCap(StartingParams startingParams, IState state)
        {
            PointF target = startingParams.TargetPoint;
            VectorF speed = state.SpeedVector;

            if (MathF.IsZero(speed.Length))
            {
                return (startingParams.MaxSpeed / startingParams.StepsPerSecond, startingParams.MaxSpeed / startingParams.StepsPerSecond);
            }

            // Moving world origin to ship
            target = target.Subtract(new VectorF(state.Position.X, state.Position.Y));
            // Rotating world such that ship points upwards
            target = target.RotateLeft(speed.Angle);

            float leftSpeedCap, rightSpeedCap;

            Func<float, float, float, float> evalSpeedCapFunc = (float x, float y, float angle) => 
            {
                var angleRad = (float)((angle / 180.0f) * Math.PI);
                float d = ((x * x) + (y * y)) / (x * (1 / (float)Math.Tan(angleRad / 2)) - y);

                float a = (d / 2.0f) * (float)Math.Tan(angleRad / 2.0f);
                float b = (d / 2.0f);
                float r = (d / 2.0f) * (float)Math.Sin(angleRad / 2.0f);
                
                return d;
            };

            try
            {
                leftSpeedCap = evalSpeedCapFunc(target.X, target.Y, startingParams.MaxLeftRotationPerStep);
                if (leftSpeedCap < 0.0f)
                    leftSpeedCap = float.NaN;
            }
            catch
            {
                leftSpeedCap = float.NaN;
            }

            try
            {
                rightSpeedCap = evalSpeedCapFunc(target.X, target.Y, startingParams.MaxRightRotationPerStep); 
                if (rightSpeedCap < 0.0f)
                    rightSpeedCap = float.NaN;
            }
            catch
            {
                rightSpeedCap = float.NaN;
            }

            return (leftSpeedCap, rightSpeedCap);
        }

        private float EvalDesiredSpeed(StartingParams startingParams, float distance)
        {
            if (distance < 0.0f)
                throw new ArgumentException("Invalid distance!");

            // Allows remodeling the optimal trajectory below ship's capabilities.
            // This adds a room to maneuver, making reaching target easier.
            // Set the value below 1.0 to enable.
            float desiredReverseThrust = startingParams.MaxReverseThrust * 1.0f;

            float brakingTime = -(startingParams.MaxSpeed / desiredReverseThrust);
            float brakingDistance = brakingTime * startingParams.MaxSpeed + (desiredReverseThrust * brakingTime * brakingTime) / 2.0f;

            if (distance > brakingDistance)
            {
                // Desired vector is towards target with maximum speed
                return startingParams.MaxSpeed;
            }
            else if (MathF.IsZero(distance))
            {
                return 0.0f;
            }
            else
            {
                // Ship should be braking to reach target
                (float t, _) = MathF.SolveQuadratic(desiredReverseThrust / 2, startingParams.MaxSpeed, -(brakingDistance - distance));

                if (float.IsNaN(t))
                {
                    // Border point between max speed and starting braking
                    return startingParams.MaxSpeed;
                }
                else
                {
                    float desiredSpeed = startingParams.MaxSpeed + desiredReverseThrust * t;
                    return desiredSpeed;
                }
            }
        }

        public Decision Decide(StartingParams startingParams, IState currentState)
        {
            // Gathering data

            VectorF shipToTarget = startingParams.TargetPoint.Subtract(currentState.Position);

            if (shipToTarget.Length < 5)
            {
                // Stop the ship

                float thrust = Math.Min(startingParams.MaxThrustPerStep, Math.Max(-startingParams.MaxThrust, -currentState.Speed));

                return new Decision(thrust, 0.0f);
            }
            else
            {
                // Rotation

                AngleF orientationDiff = currentState.Orientation.ShortestDiffTo(shipToTarget.Angle);

                bool backwards = false;
                if (Math.Abs(orientationDiff.Value) > 90.0f)
                {
                    backwards = true;
                    orientationDiff = new AngleF(orientationDiff.Value - 180.0f * Math.Sign(orientationDiff.Value));
                }

                float rotation = Math.Min(startingParams.MaxRightRotationPerStep, Math.Max(startingParams.MaxLeftRotationPerStep, orientationDiff.Value));

                // Speed

                (float leftSpeedCap, float rightSpeedCap) = EvalSpeedCap(startingParams, currentState);

                leftSpeedCap *= startingParams.StepsPerSecond;
                rightSpeedCap *= startingParams.StepsPerSecond;

                // TODO Evaluate speed cap for the turning angle

                float desiredSpeed = EvalDesiredSpeed(startingParams, shipToTarget.Length);

                if (rotation > 0.0f && !float.IsNaN(rightSpeedCap))
                {
                    desiredSpeed = Math.Min(desiredSpeed, rightSpeedCap);
                }
                else if (rotation < 0.0f && !float.IsNaN(leftSpeedCap))
                {
                    desiredSpeed = Math.Min(desiredSpeed, leftSpeedCap);
                }

                if (backwards)
                    desiredSpeed = -desiredSpeed;

                float thrust = Math.Min(startingParams.MaxThrustPerStep, Math.Max(-startingParams.MaxThrust, desiredSpeed - currentState.Speed));

                return new Decision(thrust, rotation);
            }
        }

        public void DrawDebug(State state, Decision decision, Graphics g)
        {
            AIPilotDebugData data = decision.Data as AIPilotDebugData;
            if (data == null)
                return;

            // g.DrawLine(Pens.Orange, state.Position.X, state.Position.Y, state.Position.X + data.ShipToTarget.DX, state.Position.Y + data.ShipToTarget.DY);            
        }
    }
}
