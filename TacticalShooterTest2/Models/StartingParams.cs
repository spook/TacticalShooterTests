﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest2.Models
{
    public class StartingParams
    {
        // Private fields -----------------------------------------------------

        private float maxThrust;
        private float maxReverseThrust;
        private float maxLeftRotation;
        private float maxRightRotation;

        // Public methods -----------------------------------------------------

        public StartingParams(PointF startingPoint,
            NormalizedAngleF startingAngle,
            float startingSpeed,
            float maxThrust,
            float maxSpeed,
            float maxRotation,
            PointF targetPoint,
            int stepsPerSecond,
            int seconds)
        {
            StartingPoint = startingPoint;
            StartingAngle = startingAngle;
            StartingSpeed = startingSpeed;
            MaxThrust = maxThrust;
            MaxReverseThrust = -maxThrust;
            MaxSpeed = maxSpeed;
            MaxRightRotation = maxRotation;
            MaxLeftRotation = -maxRotation;
            TargetPoint = targetPoint;
            StepsPerSecond = stepsPerSecond;
            Seconds = seconds;

            MaxLeftRotationPerStep = (2.0f * (float)Math.Asin(Math.Sin((MaxLeftRotation / 180.0f * Math.PI) / 2) / StepsPerSecond)) / (float)Math.PI * 180.0f;
            MaxRightRotationPerStep = (2.0f * (float)Math.Asin(Math.Sin((MaxRightRotation / 180.0f * Math.PI) / 2) / StepsPerSecond)) / (float)Math.PI * 180.0f;
            MaxThrustPerStep = MaxThrust / StepsPerSecond;
            MaxReverseThrustPerStep = MaxReverseThrust / StepsPerSecond;
        }

        // Public properties --------------------------------------------------

        public PointF StartingPoint { get; private set; }
        public NormalizedAngleF StartingAngle { get; private set; }
        public float StartingSpeed { get; private set; }
        public float MaxSpeed { get; private set; }
        public PointF TargetPoint { get; private set; }
        public int StepsPerSecond { get; private set; }
        public int Seconds { get; private set; }

        public float MaxReverseThrust
        {
            get => maxReverseThrust;
            private set
            {
                if (value > 0.0f)
                    throw new ArgumentException("Invalid max reverse thrust!");
                maxReverseThrust = value;
            }
        }
        public float MaxThrust
        {
            get => maxThrust;
            private set
            {
                if (value < 0.0f)
                    throw new ArgumentException("Invalid max thrust!");
                maxThrust = value;
            }
        }
        public float MaxLeftRotation
        {
            get => maxLeftRotation;
            private set
            {
                if (value > 0.0f)
                    throw new ArgumentException("Invalid max left rotation");

                maxLeftRotation = value;
            }
        }
        public float MaxRightRotation
        {
            get => maxRightRotation;
            private set
            {
                if (value < 0.0f)
                    throw new ArgumentException("Invalid max right rotation");

                maxRightRotation = value;
            }
        }

        public float MaxLeftRotationPerStep { get; private set; }
        public float MaxRightRotationPerStep { get; private set; }
        public float MaxThrustPerStep { get; private set; }
        public float MaxReverseThrustPerStep { get; private set; }
    }
}