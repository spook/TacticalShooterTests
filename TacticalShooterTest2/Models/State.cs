﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest2.Models
{
    public class State : IState
    {
        private VectorF GetSpeedVector()
        {
            if (Speed > 0.0f)
            {
                return VectorF.FromPolar(Math.Abs(Speed), Orientation);
            }
            else
            {
                return VectorF.FromPolar(Math.Abs(Speed), Orientation.Add(180.0f));
            }
        }

        public State(PointF position, NormalizedAngleF orientation, float speed)
        {
            Position = position;
            Orientation = orientation;
            Speed = speed;
        }

        public PointF Position { get; private set; }
        public NormalizedAngleF Orientation { get; private set; }
        public float Speed { get; private set; }

        public VectorF SpeedVector
        {
            get
            {
                return GetSpeedVector();
            }
        }
    }
}
