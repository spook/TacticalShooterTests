﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest3.Models;

namespace TacticalShooterTest3.Core
{
    public static class Engine
    {
        public static SimulationResult Simulate(StartingParams startingParams, IPilot pilot)
        {
            List<State> states = new List<State>();
            List<Decision> decisions = new List<Decision>();

            State state = new State(startingParams.StartingPoint, startingParams.StartingAngle, startingParams.StartingSpeed);

            states.Add(state);

            for (int i = 0; i < startingParams.Seconds; i++)
            {
                for (int j = 0; j < startingParams.StepsPerSecond; j++)
                {
                    // System.Diagnostics.Debug.WriteLine($"Current step: {i * startingParams.StepsPerSecond + j}");

                    Decision decision;
                    try
                    {
                        decision = pilot.Decide(startingParams, state);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Debug.WriteLine($"Exception during making decision ({i*startingParams.StepsPerSecond+j}): {e.Message}");
                        decision = new Decision(0.0f, 0.0f, null);
                    }
                    decisions.Add(decision);

                    float rotationChange = MathF.Clamp(startingParams.MaxLeftRotationPerStep, decision.OrientationChange, startingParams.MaxRightRotationPerStep);                    
                    float speedChange = MathF.Clamp(startingParams.MaxReverseThrustPerStep, decision.SpeedChange, startingParams.MaxThrustPerStep);

                    NormalizedAngleF newOrientation = state.Orientation.Add(rotationChange);
                    if (float.IsNaN(newOrientation.Value))
                        System.Diagnostics.Debugger.Break();
                    float newSpeed = MathF.Clamp(-startingParams.MaxSpeed, state.Speed + speedChange, startingParams.MaxSpeed);

                    PointF newPosition;
                    if (!MathF.IsZero(newSpeed))
                    {
                        VectorF scaledSpeed;
                        if (state.Speed > 0)
                            scaledSpeed = VectorF.FromPolar(state.Speed / startingParams.StepsPerSecond, state.Orientation);
                        else
                            scaledSpeed = VectorF.FromPolar(-state.Speed / startingParams.StepsPerSecond, state.Orientation.Add(180.0f));

                        newPosition = state.Position.Add(scaledSpeed);
                    }
                    else
                    {
                        // No speed means no position change
                        newPosition = state.Position;
                    }

                    state = new State(newPosition, newOrientation, newSpeed);
                    states.Add(state);
                }
            }

            return new SimulationResult(startingParams, states, decisions);
        }
    }
}
