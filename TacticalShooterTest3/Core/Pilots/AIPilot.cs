﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;
using TacticalShooterTest3.Models;

namespace TacticalShooterTest3.Core.Pilots
{
    public class AIPilot : IPilot
    {
        private const float DISTANCE_THRESHOLD = 2.0f;
        private const float ANGLE_ALIGNMENT_THRESHOLD = 2.0f;
        private const float MIN_RADIUS = 1.0f;

        private (float, float) EvalCurrentTurnCircleRadius(StartingParams startingParams, IState currentState)
        {
            float speed = Math.Abs(currentState.Speed) / startingParams.StepsPerSecond;
            float leftRotation = startingParams.MaxLeftRotationPerStep;
            float rightRotation = startingParams.MaxRightRotationPerStep;

            return ((float)(speed / Math.Sqrt(2 - 2 * Math.Cos(leftRotation / 180 * Math.PI))),
                (float)(speed / Math.Sqrt(2 - 2 * Math.Cos(rightRotation / 180 * Math.PI))));
        }

        private (PointF?, PointF?) CircleToCircleIntersection(PointF center1, float radius1, PointF center2, float radius2)
        {
            // Source: https://stackoverflow.com/questions/3349125/circle-circle-intersection-points

            float d = (center2.Subtract(center1)).Length;
            if (d > radius1 + radius2 || d < Math.Abs(radius1 - radius2))
                return (null, null);
            if (MathF.IsZero(d))
                return (null, null);

            float a = (radius1 * radius1 - radius2 * radius2 + d * d) / (2 * d);
            float h = (float)Math.Sqrt(radius1 * radius1 - a * a);

            PointF P2 = center1.Add((center2.Subtract(center1)).Multiply(a / d));
            PointF Result1 = new PointF(P2.X + (h / d) * (center2.Y - center1.Y), P2.Y - (h / d) * (center2.X - center1.X));
            PointF Result2 = new PointF(P2.X - (h / d) * (center2.Y - center1.Y), P2.Y + (h / d) * (center2.X - center1.X));

            return (Result1, Result2);
        }

        private bool IsLeft(PointF linePointA, PointF linePointB, PointF pointInQuestion)
        {
            return ((linePointB.X - linePointA.X) * (pointInQuestion.Y - linePointA.Y) - (linePointB.Y - linePointA.Y) * (pointInQuestion.X - linePointA.X)) > 0;
        }

        private bool IsLeft(VectorF vector, VectorF vectorInQuestion)
        {
            return vector.DX * vectorInQuestion.DY - vector.DY * vectorInQuestion.DX > 0;
        }

        private float EvalMaxSpeedReverse(float maxSpeed, float maxReverseThrust, float distance)
        {
            if (maxSpeed > 0.0f)
                throw new ArgumentException("Invalid max speed!");
            if (distance < 0.0f)
                throw new ArgumentException("Invalid distance!");
            if (maxReverseThrust < 0.0f)
                throw new ArgumentException("Invalid max reverse thrust!");

            return -EvalMaxSpeed(-maxSpeed, -maxReverseThrust, distance);
        }

        private float EvalMaxSpeed(float maxSpeed, float maxReverseThrust, float distance)
        {
            if (maxSpeed < 0.0f)
                throw new ArgumentException("Invalid max speed!");
            if (distance < 0.0f)
                throw new ArgumentException("Invalid distance!");
            if (maxReverseThrust > 0.0f)
                throw new ArgumentException("Invalid max reverse thrust!");

            // Allows remodeling the optimal trajectory below ship's capabilities.
            // This adds a room to maneuver, making reaching target easier.
            // Set the value below 1.0 to enable.
            float desiredReverseThrust = maxReverseThrust * 1.0f;

            float brakingTime = -(maxSpeed / desiredReverseThrust);
            float brakingDistance = -(maxSpeed * maxSpeed) / (2 * desiredReverseThrust);

            if (distance > brakingDistance)
            {
                // Desired vector is towards target with maximum speed
                return maxSpeed;
            }
            else if (MathF.IsZero(distance))
            {
                return 0.0f;
            }
            else
            {
                // Ship should be braking to reach target
                (float t, _) = MathF.SolveQuadratic(desiredReverseThrust / 2, maxSpeed, -(brakingDistance - distance));

                if (float.IsNaN(t))
                {
                    // Border point between max speed and starting braking
                    return maxSpeed;
                }
                else
                {
                    float desiredSpeed = maxSpeed + desiredReverseThrust * t;
                    return desiredSpeed;
                }
            }
        }

        private Decision MatchVector(IAngleF currentOrientation, float currentSpeed, IAngleF desiredOrientation, float desiredSpeed, StartingParams startingParams)
        {
            AngleF orientationDiff = currentOrientation.ShortestDiffTo(desiredOrientation);
            float rotation = Math.Min(startingParams.MaxRightRotationPerStep, Math.Max(startingParams.MaxLeftRotationPerStep, orientationDiff.Value));

            float thrust;
            if (Math.Abs(orientationDiff.Value) > 90)
            {
                // Brake only - direction is wrong
                thrust = startingParams.MaxReverseThrustPerStep;
            }
            else
            {
                // Accelerate or brake, depending on angle
                if (currentSpeed > desiredSpeed)
                {
                    // Brake
                    thrust = Math.Max((desiredSpeed - currentSpeed), startingParams.MaxReverseThrustPerStep);
                }
                else
                {
                    float factor = (1.0f - Math.Abs(orientationDiff.Value / 90));

                    thrust = Math.Min((desiredSpeed - currentSpeed), (startingParams.MaxThrustPerStep) * factor);
                }
            }

            return new Decision(thrust, rotation);
        }

        public Decision Decide(StartingParams startingParams, IState currentState)
        {
            VectorF unitTargetDir = VectorF.FromPolar(1.0f, new AngleF(startingParams.TargetAngle));
            VectorF targetToPosition = currentState.Position.Subtract(startingParams.TargetPoint);
            VectorF positionToTarget = new VectorF(-targetToPosition.DX, -targetToPosition.DY);

            if (targetToPosition.Length < DISTANCE_THRESHOLD)
            {
                // Near the target - just rotate towards required direction and stop
                AngleF orientationDiff = currentState.Orientation.ShortestDiffTo(new AngleF(startingParams.TargetAngle));
                float rotation = Math.Min(startingParams.MaxRightRotationPerStep, Math.Max(startingParams.MaxLeftRotationPerStep, orientationDiff.Value));

                float thrust = Math.Min(startingParams.MaxThrustPerStep, Math.Max(startingParams.MaxReverseThrustPerStep, -currentState.Speed));
                return new Decision(thrust, rotation, new AIPilotDebugData(0));
            }

            bool forward = !(Math.Abs(targetToPosition.Angle.ShortestDiffTo(new AngleF(startingParams.TargetAngle)).Value) < 90.0f);

            if ((forward && currentState.Speed < 0) || (!forward && currentState.Speed > 0))
            {
                AngleF orientationDiff = currentState.Orientation.ShortestDiffTo(forward ? positionToTarget.Angle : positionToTarget.Angle.Add(180.0f));
                float rotation = Math.Min(startingParams.MaxRightRotationPerStep, Math.Max(startingParams.MaxLeftRotationPerStep, orientationDiff.Value));
                float thrust;
                if (forward)
                    thrust = Math.Min(-currentState.Speed, startingParams.MaxThrustPerStep);
                else
                    thrust = Math.Max(-currentState.Speed, startingParams.MaxReverseThrustPerStep);

                return new Decision(thrust, rotation, null);
            }
            else
            {
                float courseDiff = positionToTarget.Angle.ShortestDiffTo(unitTargetDir.Angle).Value;
                float orientationDiff = currentState.Orientation.ShortestDiffTo(unitTargetDir.Angle).Value;
                if (Math.Abs(courseDiff) < ANGLE_ALIGNMENT_THRESHOLD && Math.Abs(orientationDiff) < ANGLE_ALIGNMENT_THRESHOLD)
                {
                    // Already on valid trajectory - just fly towards it

                    return FlyTowardsTarget(startingParams, currentState, forward);
                }
                else
                {
                    (float leftRadius, float rightRadius) = EvalCurrentTurnCircleRadius(startingParams, currentState);

                    bool left = IsLeft(unitTargetDir, targetToPosition);

                    VectorF targetDirNormal;
                    PointF circleCenter;
                    float radius;

                    if (left)
                    {
                        targetDirNormal = new VectorF(-unitTargetDir.DY, unitTargetDir.DX);
                        radius = leftRadius;
                    }
                    else
                    {
                        targetDirNormal = new VectorF(unitTargetDir.DY, -unitTargetDir.DX);
                        radius = rightRadius;
                    }

                    if (radius < MIN_RADIUS)
                    {
                        return FlyTowardsTarget(startingParams, currentState, forward);
                    }

                    // System.Diagnostics.Debug.WriteLine($"Radius: {radius}");

                    circleCenter = startingParams.TargetPoint.Add(targetDirNormal.Multiply(radius));

                    VectorF positionToCircleCenter = circleCenter.Subtract(currentState.Position);
                    float positionToCircleCenterDist = positionToCircleCenter.Length;
                    float distanceFromCircle = positionToCircleCenterDist - radius;

                    if (distanceFromCircle < 0.0f)
                    {
                        // *** Inside circle ***

                        // Slow down to be able to fly to circle's tangent
                        return new Decision(forward ? startingParams.MaxReverseThrustPerStep : startingParams.MaxThrustPerStep, 0.0f, new AIPilotDebugData(2));
                    }
                    else
                    {
                        // Outside or on the circle

                        // Find tangent points on circle
                        float positionToCircleTangentDist = (float)Math.Sqrt(positionToCircleCenterDist * positionToCircleCenterDist - radius * radius);
                        (PointF? tangentPoint1, PointF? tangentPoint2) = CircleToCircleIntersection(currentState.Position, positionToCircleTangentDist, circleCenter, radius);

                        if (tangentPoint1 == null || tangentPoint2 == null)
                            throw new InvalidOperationException("Error in algorithm - no tangents outside circle!");

                        // Choose valid tangent
                        bool tangentPoint1IsLeft = IsLeft(positionToCircleCenter, currentState.Position.Subtract(tangentPoint1.Value));
                        if (!forward)
                            tangentPoint1IsLeft = !tangentPoint1IsLeft;

                        PointF validTangent;
                        if ((left && tangentPoint1IsLeft) || (!left && !tangentPoint1IsLeft))
                            validTangent = tangentPoint1.Value;
                        else
                            validTangent = tangentPoint2.Value;

                        // Calculate distance on-circle

                        float distance = positionToTarget.Length;
                        float maxSpeed = forward ? EvalMaxSpeed(startingParams.MaxSpeed, startingParams.MaxReverseThrust, distance) : EvalMaxSpeedReverse(-startingParams.MaxSpeed, startingParams.MaxThrust, distance);

                        VectorF positionToTangent = validTangent.Subtract(currentState.Position);

                        IAngleF desiredAngle = forward ? positionToTangent.Angle : positionToTangent.Angle.Add(180.0f);
                        float desiredSpeed = maxSpeed;

                        Decision decision = MatchVector(currentState.Orientation, currentState.Speed, desiredAngle, desiredSpeed, startingParams);

                        // If in range of circle, don't speed up anymore
                        if (positionToTangent.Length < radius)
                            decision.SpeedChange = forward ? Math.Min(0, decision.SpeedChange) : Math.Max(0, decision.SpeedChange);

                        AIPilotDebugData data;
                        data = new AIPilotDebugData(4, circleCenter, radius, validTangent);

                        return new Decision(decision.SpeedChange, decision.OrientationChange, data);
                    }                    
                }
            }
        }

        private Decision FlyTowardsTarget(StartingParams startingParams, IState currentState, bool forward)
        {
            VectorF positionToTarget = startingParams.TargetPoint.Subtract(currentState.Position);

            float dist = positionToTarget.Length;
            float maxSpeed = forward ? EvalMaxSpeed(startingParams.MaxSpeed, startingParams.MaxReverseThrust, dist) : EvalMaxSpeedReverse(-startingParams.MaxSpeed, startingParams.MaxThrust, dist);

            IAngleF desiredAngle = forward ? positionToTarget.Angle : positionToTarget.Angle.Add(180.0f);
            float desiredSpeed = maxSpeed;

            Decision decision = MatchVector(currentState.Orientation, currentState.Speed, desiredAngle, desiredSpeed, startingParams);
            return new Decision(decision.SpeedChange, decision.OrientationChange, new AIPilotDebugData(1));
        }

        public void DrawDebug(State state, Decision decision, Graphics g)
        {
            AIPilotDebugData data = decision.Data as AIPilotDebugData;
            if (data == null)
                return;

            switch (data.Mode)
            {
                case 0:
                    {
                        // Stopping
                        g.DrawEllipse(Pens.Green, state.Position.X - 25, state.Position.Y - 25, 50, 50);
                        break;
                    }
                case 1:
                    {
                        // Fly towards target
                        g.DrawEllipse(Pens.Yellow, state.Position.X - 25, state.Position.Y - 25, 50, 50);
                        break;
                    }
                case 2:
                    {
                        // Inside circle
                        g.DrawEllipse(Pens.Blue, state.Position.X - 25, state.Position.Y - 25, 50, 50);
                        break;
                    }
                case 3:
                    {
                        // On circle
                        g.DrawEllipse(Pens.Magenta, state.Position.X - 25, state.Position.Y - 25, 50, 50);
                        break;
                    }
                case 4:
                    {
                        // Outside circle
                        g.DrawEllipse(Pens.Black, state.Position.X - 25, state.Position.Y - 25, 50, 50);
                        break;
                    }
            }

            if (data.CircleCenter != null && data.Radius != null)
            {
                g.DrawEllipse(Pens.Red, data.CircleCenter.Value.X - data.Radius.Value, data.CircleCenter.Value.Y - data.Radius.Value, data.Radius.Value * 2, data.Radius.Value * 2);
            }

            if (data.Tangent != null)
            {
                g.DrawLine(Pens.Red, state.Position, data.Tangent.Value);
            }

            if (data.DesiredPoint != null)
            {
                g.FillEllipse(Brushes.Black, data.DesiredPoint.Value.X - 2, data.DesiredPoint.Value.Y - 2, 4, 4);
            }

            // g.DrawLine(Pens.Orange, state.Position.X, state.Position.Y, state.Position.X + data.ShipToTarget.DX, state.Position.Y + data.ShipToTarget.DY);            
        }
    }
}
