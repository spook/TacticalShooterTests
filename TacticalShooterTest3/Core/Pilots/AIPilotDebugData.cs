﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest3.Core.Pilots
{
    public class AIPilotDebugData
    {
        private int mode;
        private PointF? center;
        private float? radius;
        private PointF? tangent;
        private PointF? desiredPoint;
        
        public AIPilotDebugData(int mode, PointF? leftCenter = null, float? leftRadius = null, PointF? tangent = null, PointF? desiredPoint = null)
        {
            this.mode = mode;
            this.center = leftCenter;
            this.radius = leftRadius;
            this.tangent = tangent;
            this.desiredPoint = desiredPoint;
        }

        public PointF? CircleCenter { get => center; set => center = value; }
        public float? Radius { get => radius; set => radius = value; }
        public PointF? Tangent { get => tangent; set => tangent = value; }
        public int Mode { get => mode; set => mode = value; }
        public PointF? DesiredPoint { get => desiredPoint; set => desiredPoint = value; }
    }
}
