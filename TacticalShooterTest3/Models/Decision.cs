﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TacticalShooterTest3.Models
{
    public class Decision
    {
        public Decision(float speedChange, float orientationChange, Object data = null)
        {
            SpeedChange = speedChange;
            OrientationChange = orientationChange;
            Data = data;
        }

        public float SpeedChange { get; set; }
        public float OrientationChange { get; set; }        
        public Object Data { get; set; }
    }
}
