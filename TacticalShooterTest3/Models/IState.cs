﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest3.Models
{
    public interface IState
    {
        PointF Position { get; }
        NormalizedAngleF Orientation { get; }
        float Speed { get; }
        VectorF SpeedVector { get; }
    }
}
