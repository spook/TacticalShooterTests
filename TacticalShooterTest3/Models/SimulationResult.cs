﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TacticalShooter.Common;

namespace TacticalShooterTest3.Models
{
    public class SimulationResult
    {
        public SimulationResult(StartingParams startingParams, List<State> states, List<Decision> decisions)
        {
            StartingParams = startingParams;
            States = states;
            Decisions = decisions;
        }

        public StartingParams StartingParams { get; private set; }
        public List<State> States { get; private set; }
        public List<Decision> Decisions { get; private set; }
    }
}
