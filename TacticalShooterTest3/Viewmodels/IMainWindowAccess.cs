﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TacticalShooterTest3.Viewmodels
{
    public interface IMainWindowAccess
    {
        SizeF GetPreviewSize();
        void DrawPreview(Bitmap bitmap);
    }
}
